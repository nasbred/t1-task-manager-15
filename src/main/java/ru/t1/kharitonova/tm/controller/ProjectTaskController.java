package ru.t1.kharitonova.tm.controller;

import ru.t1.kharitonova.tm.api.service.IProjectTaskService;
import ru.t1.kharitonova.tm.util.TerminalUtil;
import ru.t1.kharitonova.tm.api.controller.IProjectTaskController;


public final class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID: ");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.bindTaskToProject(projectId, taskId);
    }

    @Override
    public void unbindTaskToProject() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID: ");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.unbindTaskToProject(projectId,taskId);
    }

}
